<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome3');
});

Auth::routes([
    'register'=>false
]);

Route::get('/home', 'HomeController@index')->name('home');





    // Route::resource('users', 'UsersController');

    Route::resource('profile', 'ProfilesController');

    Route::resource('dashboard', 'DashboardsController'); 
    
    // Route::get('datalog/getdata','DataLogsController@getdata'); 
    Route::resource('datapegawai', 'DataPegawaisController');
    Route::get('datapegawai/{id}/hapus', 'DataPegawaisController@destroy');
    
    
    
    // Route::get('datalog/getdata','DataLogsController@getdata'); 
    Route::get('rekapankehadiran/{nama}','RekapanKehadiransController@detail');
    Route::resource('rekapankehadiran', 'RekapanKehadiransController');
    
    Route::get('rekapanketerlambatan/getdata','RekapanKeterlambatansController@getdata'); 
    Route::get('rekapanketerlambatan/{nama}','RekapanKeterlambatansController@detail');
    Route::resource('rekapanketerlambatan', 'RekapanKeterlambatansController');
    
    
    
    Route::resource('rekapandataabsensi', 'RekapanDataAbsensisController');
    
    
    Route::get('datalog/getdata','DataLogsController@getdata'); 
    Route::resource('datalog', 'DataLogsController');
    
    
    // Route::get('datalog/getdata','DataLogsController@getdata'); 
    Route::resource('datapenggajian', 'DataPenggajiansController');
    
    // Route::get('datalog/getdata','DataLogsController@getdata'); 
   
    Route::resource('importdataabsensi', 'UploadController');  
      
       
    Route::resource('masterdata/jabatan', 'JabatansController');
    Route::get('masterdata/jabatan/{id}/hapus', 'JabatansController@destroy');
    
    
    Route::resource('masterdata/insentif', 'InsentifsController');
    Route::get('masterdata/insentif/{id}/hapus', 'InsentifsController@destroy');
    
    Route::resource('masterdata/roles', 'RolesController');
    
    
    // Route::get('datalog/getdata','DataLogsController@getdata'); 
    Route::resource('masterdata/buataccount', 'BuatAccountsController');
    Route::get('masterdata/buataccount/{id}/hapus', 'BuatAccountsController@destroy');
    
    
    
    
    
    // // // routing groub bendahara
    // Route::prefix('bendahara')->group(function () {
    //     Route::resource('rekapankehadiran', 'RekapanKehadiransController');
    //     Route::resource('rekapanketerlambatan', 'RekapanKeterlambatansController');
    //     Route::resource('laporanpenggajian', 'LaporanPenggajiansController');
    // });
    
    
    // // routing groub pegawai
        Route::resource('pegawai/rekapankehadiran', 'PegawaiRekapanKehadiransController');
        Route::resource('pegawai/rekapanketerlambatan', 'PegawaiRekapanKeterlambatansController');
        Route::resource('pegawai/laporanpenggajian', 'PegawaiLaporanPenggajiansController');  
        Route::resource('pegawai/rekapdataabsensi', 'PegawaiRekapanDataAbsensisController');  
 