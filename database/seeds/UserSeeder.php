<?php

use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateorcreate ([
        'name' => 'Sigapei',
        'email' => 'sigapei@gmail.com',
        'password' => '$2y$12$RNm5zbyIFbK88MaYHyFGzuaqmn7MWvDw5wxia0hCHLROJVIkjMY.e',
        'role_id'=> '1',
        'avatar' => 'tatausaha.img',
        'status' => 'Aktif'
        ]);
    }
}
