<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::updateOrCreate([
            'name' => 'TATAUSAHA'
        ]);
        Role::updateOrCreate([
            'name' => 'BENDAHARA'
        ]);
        Role::updateOrCreate([
            'name' => 'KEPSEK'
        ]);
        Role::updateOrCreate([
            'name' => 'PEGAWAI'
        ]);
    }
}
