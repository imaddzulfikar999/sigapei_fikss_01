<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsensisTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'absensis';

    /**
     * Run the migrations.
     * @table absensis
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('Pegawai_id', 191)->nullable()->default(null);
            $table->integer('Periode')->nullable()->default(null);
            $table->date('Tanggal')->nullable()->default(null);
            $table->time('Jam_Masuk')->nullable()->default(null);
            $table->time('Jam_Keluar')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
