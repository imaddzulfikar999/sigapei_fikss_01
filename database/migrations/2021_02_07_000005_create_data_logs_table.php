<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataLogsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'data_logs';

    /**
     * Run the migrations.
     * @table data_logs
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('Periode', 191)->nullable()->default(null);
            $table->string('Tanggal', 191)->nullable()->default(null);
            $table->string('Pegawai_id', 191)->nullable()->default(null);
            $table->string('Nama_Pegawai', 191)->nullable()->default(null);
            $table->string('Waktu_Absen', 191)->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
