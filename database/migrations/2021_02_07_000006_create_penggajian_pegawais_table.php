<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenggajianPegawaisTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'penggajian_pegawais';

    /**
     * Run the migrations.
     * @table penggajian_pegawais
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('id', 191);
            $table->date('Tanggal');
            $table->string('Pegawai_id', 191)->nullable()->default(null);
            $table->string('Gaji_Pokok', 191)->nullable()->default(null);
            $table->integer('Insentif_id');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
