<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuatAccounts extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'name','email','password','role_id','avatar','status' ];
}
