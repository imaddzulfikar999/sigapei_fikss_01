<?php

namespace App\Http\Controllers;

use App\DataLogs;
use App\DataPegawais;
use App\Imports\AbsensiImport;
use App\Imports\TestImport;
use App\Models\Datalog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UploadController extends Controller
{
    public function index()
    {
        return view('ImportDataAbsensi.index');
    }

    public function store()
    {
        $datas = Excel::toArray(new TestImport(),request()->file('fileupload'));
        // dd($datas[0]);
        $periode = $datas[0][2][2];
        $pecah_periode = explode('~',$periode);
        $tahun = explode('/',$pecah_periode[0]);
        $tgl_selesai = substr($pecah_periode[1],1,2);
   
        $absensi = [];
            foreach ($datas[0] as $key => $rows) {
                if ($key > 3){
                    if ($key % 2 == 0) {
                        foreach ($datas[0][($key+1)] as $k2 => $row) {
                            if ($k2 < $tgl_selesai){
                                $tgl = ($k2 + 1);
                                if(strlen($tgl) < 2){
                                    $tgl = '0'.$tgl;
                                }
                                $waktu = preg_split("/[\s]+/",$row);

                                $absensi[$rows[2].'|'.$rows[10]][] = [
                                    'tahun' => trim($tahun[2]),
                                    'bulan' => trim($tahun[1]),
                                    'tanggal' => $tgl,
                                    'waktu' => $waktu,
                                    'count' => count($waktu)
                                ];
                             
                            }
                        }
                    }
                }
            }
//             echo '<pre>';
//    echo print_r($absensi);
//    echo '</pre>';
        foreach ($absensi as $key => $values) {
            foreach ($values as $value) {
                $pegawai = explode('|',$key);
                //cek jika nomor absen ada di tabel pengawai
                if(DataPegawais::where('Nomor_Absen',$pegawai[0])->exists()){
                    DataLogs::updateOrCreate([
                    'tanggal' =>  $value['tahun'].'-'.$value['bulan'].'-'.$value['tanggal'],
                    'nama' => $pegawai[1],
                    'no_absen' => $pegawai[0],
                    'jam_masuk' => $value['count'] > 1 ? $value['waktu'][0] : null,
                    'jam_keluar' => $value['count'] > 2 ? $value['waktu'][1] : null,
                    ]);
                }else{
                    DataPegawais::updateOrCreate([
                        'Name' => $pegawai[1],
                        'Nomor_Absen' => $pegawai[0]
                    ]);
                    
                    DataLogs::updateOrCreate([
                        'tanggal' =>  $value['tahun'].'-'.$value['bulan'].'-'.$value['tanggal'],
                        'nama' => $pegawai[1],
                        'no_absen' => $pegawai[0],
                        'jam_masuk' => $value['count'] > 1 ? $value['waktu'][0] : null,
                        'jam_keluar' => $value['count'] > 2 ? $value['waktu'][1] : null,
                        ]);
                }
                
            }
        }
    }
}