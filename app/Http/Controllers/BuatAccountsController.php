<?php


use App\BuatAccounts;
namespace App\Http\Controllers;

use App\User;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;

class BuatAccountsController extends Controller
{
    public function index()
    {
         
        $Users = User::all();
       
        
        return view('BuatAccount.index', compact('Users'));
        
     }
     
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('BuatAccount.create');
    }
    
    public function store(Request $request)
    {
        User::create ([
            'name' => $request->name ,
            'email' =>  $request->email,
            'password' => bcrypt($request->password),
            'role_id'=>  $request->roles,
            'avatar' =>  $request->avatar,
            'status' =>  $request->status
            ]);
        return redirect('masterdata/buataccount');
    }
    
    public function edit($id)
    {
        $edit = User::find($id);
        return view('BuatAccount.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = User::find($id);
        $edit->name = $request->name;
        if ($request->has('password')) {
        $edit->password = bcrypt($request->password);
        }
        $edit->email = $request->email;
        $edit->role_id = $request->roles;
        $edit->avatar = $request->avatar;
        $edit->status = $request->status;
        
        $edit->update();
        
        return redirect('masterdata/buataccount');
    }
    
    public function destroy($id)
    {
        $Users = User::find($id);
        $Users->delete();

        return redirect()->back()->with('message', 'masterdata/buataccount deleted.');
    }

}
