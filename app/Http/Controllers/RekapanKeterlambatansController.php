<?php


use App\RekapanKeterlambatans;

namespace App\Http\Controllers;
use Yajra\Datatables\Datatables;

use App\DataLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RekapanKeterlambatansController extends Controller
{
    public function index()
    {
        $keterlambatan = DataLogs::where('jam_masuk', '>', '07:15:00')->get();
        return view('RekapanKeterlambatan.index', compact('keterlambatan'));
    }

    public function getdata()
    {
        $keterlambatan = DataLogs::select(['nama', DB::raw('COUNT(*) AS telat')])->where('jam_masuk', '>', '07:15:00')->groupBy('nama')->get();

        return Datatables::of($keterlambatan)
            ->addIndexColumn()
            ->make(true);
    }
    
    public function detail($nama)
    {
        $keterlambatan = DataLogs::select(['tanggal','nama', 'jam_masuk'])
                            ->where('jam_masuk', '>', '07:15:00')
                            ->where('nama', $nama)
                            ->groupBy('tanggal','nama','jam_masuk')->get();

        // return Datatables::of($keterlambatan)
        //     ->addIndexColumn()
        //     ->make(true);
        return view('RekapanKeterlambatan.detail', compact('keterlambatan','nama'));
    }
    
}
