<?php


namespace App\Http\Controllers;

use App\Insentifs;
use Illuminate\Http\Request;



class InsentifsController extends Controller
{
    public function index()
    {
         
    //    $insentif= Insentifs::all();
        $data = Insentifs::all();
              
        return view('Insentif.index', compact('data'));
        
     }
     
    //  public function getdata()
    //  {
    //      $data = Insentifs::get();
  
    //      return Datatables::of($data)
    //      ->addIndexColumn()
    //      ->make(true);
    //  } 
     
        
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Insentif.create');
    }
    
    public function store(Request $request)
    {
        Insentifs::create ([
            'name' => $request->name ,
            'tj_insentif' =>  $request->tj_insentif,
            ]);
        return redirect('masterdata/insentif');
    }
    
    public function edit($id)
    {
        $edit = Insentifs::find($id);
        return view('Insentif.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = Insentifs::find($id);
        $edit->name = $request->name;
        $edit->tj_insentif = $request->tj_insentif;
        $edit->update();
        
        return redirect('masterdata/insentif');
    }
    
    public function destroy($id)
    {
        $insentif = Insentifs::find($id);
        $insentif->delete();

        return redirect()->back()->with('message', 'masterdata/insentif deleted.');
    }
}
