<?php


use App\RekapanKehadirans;
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RekapanKehadiransController extends Controller
{
    public function index()
    
    {
        $kehadiran = DB::table(DB::raw("SELECT *, (SELECT COUNT(id) FROM data_log WHERE no_absen = a.Nomor_Absen) AS jml_hadir,
                                         (SELECT COUNT(id) FROM data_log WHERE no_absen = a.Nomor_Absen AND jam_masuk <= '07:15:00') 
                                          AS jml_tepat_waktu FROM `pegawais` a"));
                   
        return view('RekapanKehadiran.index', compact('kehadiran'));
    }
    
    
    // public function getdata()
    // {
    //     $kehadiran = Datalogs::SELECT *, (SELECT COUNT(id) FROM data_log WHERE no_absen = a.Nomor_Absen) AS jml_hadir,
    //                             (SELECT COUNT(id) FROM data_log WHERE no_absen = a.Nomor_Absen AND jam_masuk <= '07:15:00') 
    //                             AS jml_tepat_waktu FROM `pegawais` a;
            
    //         return Datatables::of($kehadiran)
    //         ->addIndexColumn()
    //         ->make(true);
    // }
    
}
