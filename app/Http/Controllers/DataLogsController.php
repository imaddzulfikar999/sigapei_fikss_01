<?php



namespace App\Http\Controllers;
use App\DataLogs;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class DataLogsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         
    //    $insentif= Insentifs::all();
                   
        return view('DataLog.index');
        
     }
     
     public function getdata()
    {
        $data = DataLogs::get();
 
        return Datatables::of($data)
        ->addIndexColumn()
        ->make(true);
    } 
     
}
