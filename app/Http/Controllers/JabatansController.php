<?php

namespace App\Http\Controllers;

use App\Jabatans;
use Illuminate\Http\Request;

class JabatansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         
    //     return view('DataPenggajian.Pegawai.index');
    // //    $insentif= Insentifs::all();
        $data = Jabatans::all();
              
        return view('Jabatan.index', compact('data'));
        
     }
     
        
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Jabatan.create');
    }
    
    public function store(Request $request)
    {
        jabatans::create ([
            'name' => $request->name ,
            'tj_jabatan' =>  $request->tj_jabatan,
            ]);
        return redirect('masterdata/jabatan');
    }
    
    public function edit($id)
    {
        $edit = jabatans::find($id);
        return view('Jabatan.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = Jabatans::find($id);
        $edit->name = $request->name;
        $edit->tj_jabatan = $request->tj_jabatan;
        $edit->update();
        
        return redirect('masterdata/jabatan');
    }
    
    public function destroy($id)
    {
        $jabatan = Jabatans::find($id);
        $jabatan->delete();

        return redirect()->back()->with('message', 'masterdata/jabatan deleted.');
    }
}
