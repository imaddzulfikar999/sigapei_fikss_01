<?php


use App\ImportDataAbsensis;
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImportDataAbsensisController extends Controller
{
    
    public function index()
    {
        return view('ImportDataAbsensi.index');
    }

    public function store()
    {
        $datas = Excel::toArray(new AbsensiImport(),request()->file('fileupload'));
        // dd($datas[0]);
        $periode = $datas[0][2][2];
        $tgl_cetak = $datas[0][2][28];
        $arr_bulan = $datas[0][3];
        $pecah_periode = explode('~',$periode);
        $tgl_start = substr($pecah_periode[0],0,2);
        $tahun = explode('/',$pecah_periode[0]);
        $tgl_selesai = substr($pecah_periode[1],1,2);
        echo 'Periode : '.$periode.'<br/>';
        echo 'Tanggal Cetak : ' . $tgl_cetak . '<br/>';
        echo 'Mulai Tanggal : ' . $tgl_start . '<br/>';
        echo 'Sampai Tanggal : ' . $tgl_selesai . '<br/>';
        echo 'Sampai Tanggal : ' . $tahun[2] . '<br/>';
        echo '<table border="1" style="width:100%;" cellpadding="0" cellspacing="1">';
        echo '<thead>';
        echo '<tr>';
        echo '<th>Nama Lengkap</th>';
        $absensi = [];
        foreach ($arr_bulan as $k => $row) {
            if ($k < $tgl_selesai){
                echo '<th>' . $row . '</th>';
            }
        }
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';
            foreach ($datas[0] as $key => $rows) {
                if ($key > 3){
                    echo '<tr>';
                    if ($key % 2 == 0) {
                        echo '<td>'.$rows[10].'</td>';
                        foreach ($datas[0][($key+1)] as $k2 => $row) {
                            if ($k2 < $tgl_selesai){
                                echo '<td>' . $row . '</td>';
                                $bulan_tahun = trim($tahun[2]).'-'.trim($tahun[1]);
                                $tgl = ($k2 + 1);
                                if(strlen($tgl) < 2){
                                    $tgl = '0'.$tgl;
                                }
                                $tanggaldb = $bulan_tahun.'-'.$tgl;
                                $waktu = explode(PHP_EOL,$row);

                                $absensi[$rows[10]][] = [
                                    'tahun' => trim($tahun[2]),
                                    'bulan' => trim($tahun[1]),
                                    'tanggal' => $tgl,
                                    'waktu' => $waktu,
                                    'count' => count($waktu)
                                ];
                                // $absensi[$rows[10]][] = $row;
                                //     Datalog::updateOrCreate([
                                //         'tanggal' => $tanggaldb,
                                //         'nama' => $rows[10],
                                //         'jam_masuk' => isset($waktu[0]) ? $waktu[0] : null,
                                //         'jam_keluar' => isset($waktu[1]) ? $waktu[1] : null
                                //     ]);
                            }
                        }
                    }
                    echo '</tr>';
                }
            }
        echo '</tbody>';
        echo '</table>';
       echo '<pre>';
        echo print_r($absensi);
        echo '</pre>';

        foreach ($absensi as $key => $values) {
            foreach ($values as $value) {
                $datalogs = new DataLogs();
                $datalogs->tanggal = $value['tahun'].'-'.$value['bulan'].'-'.$value['tanggal'];
                $datalogs->nama = $key;
                $datalogs->jam_masuk = $value['count'] > 1 ? $value['waktu'][0] : null;
                $datalogs->jam_keluar = $value['count'] > 2 ? $value['waktu'][1] : null;
                $datalogs->save();
            }
        }
    }
    }
}
