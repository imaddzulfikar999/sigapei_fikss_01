<?php




namespace App\Http\Controllers;

use App\DataPegawais;
use Illuminate\Http\Request;

class DataPegawaisController extends Controller
{
    public function index()
    {
         
    //    return view('DataPegawai.index'); 
        $data = DataPegawais::all();
         
        return view('DataPegawai.index', compact('data'));
        
     }
     
        
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('DataPegawai.create');
    }
    
    public function store(Request $request)
    {
        DataPegawais::create([
            'Name' => $request->Name ,
            'Nomor_Absen' =>  $request->Nomor_Absen,
            'Jabatan_Id' =>  $request->Jabatan_Id,
            'Tanggal_Masuk' =>  $request->Tanggal_Masuk
            ]);
        return redirect('datapegawai');
    }
    
    public function edit($id)
    {
        $edit = DataPegawais::find($id);
        return view('DataPegawai.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
   
        $edit = DataPegawais::find($id);
        $edit->Name = $request->Name;
        $edit->Nomor_Absen =  $request->Nomor_Absen;
        $edit->Jabatan_Id =  $request->Jabatan_Id;
        $edit->Tanggal_Masuk =  $request->Tanggal_Masuk;
       
        $edit->update();
        
        return redirect('datapegawai');
    }
    
    
    // public function detail()
    // {
    //     return view('DataPegawai.detail');
    // }

    
    public function destroy($id)
    {
        $datapegawai = DataPegawais::find($id);
        $datapegawai->delete();

        return redirect()->back()->with('message', 'datapegawai deleted.');
    }
}
