<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatans extends Model
{
    protected $table = 'jabatans';
    protected $fillable = [
        'name','tj_jabatan' ];
}
