<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPegawais extends Model
{
    protected $table='pegawais';
    
    protected $fillable = [
    'Name','Nomor_Absen','Jabatan_Id', 'Tanggal_Masuk'
    ];
}
