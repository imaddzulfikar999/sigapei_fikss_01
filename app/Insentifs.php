<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insentifs extends Model
{
    protected $table='insentifs';
    
    protected $fillable = ['name','tj_insentif'];
}
