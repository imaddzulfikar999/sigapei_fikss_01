<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataLogs extends Model
{
    protected $table='data_log';
    
    protected $fillable = ['tanggal','nama','jam_masuk','jam_keluar','no_absen'];
    
    public $timestamps = false;
}
