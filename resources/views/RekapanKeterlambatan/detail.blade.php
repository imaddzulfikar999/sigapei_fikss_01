@extends('layout.app')
@section('content')
       
  <div class="#" role="main">
          <!-- top tiles -->
          <div class="card">
            <h5 class="card-header">List Detail Rekapan Keterlambatan {{ $nama }}  <a href="{{url('rekapanketerlambatan')}}" class="btn btn-outline-danger" style="float: right;"> Batal </a></h5>
                <div class="card-body">
                <table id="data_log" class="table table-bordered table-striped" center>
              		<thead>                      
              			<tr align="center">
              				<th>No</th>
                      <th>Tanggal</th>	
              				<th>Jam Masuk</th>
                     			
              			</tr>
              		</thead>
              	<tbody>
		  
                  @foreach($keterlambatan as $t)
                  <tr align="center">
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $t->tanggal}}</td>
                    <td>{{ $t->jam_masuk}}</td>
                  </tr>
                  
               @endforeach
    	</tbody>
    	</table>
                </div>
          </div>
  </div>
    	
    	
@endsection

