@extends('layout.app')
@section('content')
       
  <div class="#" role="main">
          <!-- top tiles -->
          <div class="card">
            <h5 class="card-header">List Rekapan Keterlambatan  </h5>
            
                
                <div class="card-body ">
					<h6>Pilih Bulan Dan Tahun  </h6>
						
					  <div class="col-md-4">
								<select class="form-control @error('roles') is-invalid @enderror" name="Pilih Bulan">
									   
								  <option value="">Pilih Bulan</option>
								  <option value="Januari">Januari</option>
								  <option value="Februari">Februari</option>
								  <option value="Maret">Maret</option>
								  <option value="April">April</option>
								
								
							   </select>
						</div>
						<div class="col-md-4">
							<select class="form-control @error('roles') is-invalid @enderror" name="Pilih Tahun">
								   
							  <option value="">Pilih Tahun</option>
							  <option value="2020">2020</option>
							  <option value="2021">2021</option>
							  <option value="2022">2022</option>
							  <option value="2023">2023</option>
							  <option value="2024">2024</option>
							  <option value="2025">2025</option>
							  <option value="2026">2026</option>
						   </select>
						   </div>
						   <h5>Total Keterlambatan :  .... X</h5>
					  </div>
					 
					  
				
							  <table id="data_log" class="table table-bordered table-striped" center>
									<thead>                      
										<tr align="center">
											<th>No</th>
											<th>Tanggal</th>
											<th>Jam Masuk</th>
											   
										</tr>
									</thead>
								<tbody>
						
					  </tbody>
					  </table>
					  
		
				</div>
		  </div>
 
    	
@endsection