@extends('layout.app')
@section('content')
       
  <div class="#" role="main">
          <!-- top tiles -->
          <div class="card">
            <h5 class="card-header">List Rekapan Keterlambatan </h5>
                <div class="card-body">
                <table id="data_log" class="table table-bordered table-striped" center>
              		<thead>                      
              			<tr align="center">
              				<th>No</th>
              				<th>Name</th>
              				<th>Rekapan Keterlambatan</th>
                      <th>Aksi </th>				
              			</tr>
              		</thead>
              	<tbody>
		
    	</tbody>
    	</table>
    	
    	
@endsection

@section('js')
<script>
	$(document).ready(function () {
	   $('#data_log').DataTable({
		   processing: true,
		   serverSide: true,
		   ajax: '{{url("rekapanketerlambatan/getdata")}}',
		   columns: [
			   
			   {
				   data: 'DT_RowIndex',
				   name: 'DT_RowIndex',
				   "sClass": "text-center"
			   },
			   {
				   data: 'nama',
				   name: 'nama',
				   "sClass": "text-center"
			   },
			   {
				   data: 'telat',
				   name: 'telat',
				   "sClass": "text-center"
			   },
			   {
			        target : 3,
			        "sClass": "text-center",
			        data: 'nama',
			        render: function(data,type,row){
			            return `<a href="/rekapanketerlambatan/${data}" class="btn btn-outline-primary btn-sm">Detail</a>`; 
						    
			        }
			   }
		   ]
	   });
   });
</script>

@endsection