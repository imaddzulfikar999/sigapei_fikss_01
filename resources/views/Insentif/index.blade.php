@extends('layout.app')
@section('content')
       
  <div class="#" role="main">
          <!-- top tiles -->
          <div class="card">
            <h5 class="card-header">List Data Insentif  <a href="{{url('masterdata/insentif/create')}}" class="btn btn-outline-primary" style="float: right;">Tambah Data</a></h5>
                <div class="card-body">
	                <table id="masterdata_insentif" class="table table-bordered table-striped" center>
	              		<thead>                      
	              			<tr align="center">
	              				<th>No</th>
	              				<th>Name</th>
	              				<th>Tunjangan Insentif</th>
	                      <th>Aksi </th>				
	              			</tr>
	              		</thead>
	              	<tbody>
		
				          @foreach($data as $i)
				            <tr align="center">
				    				  <th scope="row">{{ $loop->iteration }}</th>
				    			  	<td>{{ $i->name}}</td>
									<td>{{ $i->tj_insentif}}</td>
				              <td>
				              
				              <a href="{{url('masterdata/insentif/'.$i->id.'/edit')}}" class="btn btn-outline-warning btn-sm">Edit</a>
				              
				              <a href="{{url('masterdata/insentif/'.$i->id.'/hapus')}}" class="btn btn-outline-danger btn-sm">Hapus</a>
				              
				              </td>	
						        </tr>
					       @endforeach
    	            </tbody>
    	           </table>
    	
				</div>
		  </div>
  </div>
@endsection


@section('js')
<script>
	$(document).ready(function () {
	   $('#masterdata_insentif').DataTable();
   });
</script>

@endsection