@extends('layout.app')
@section('content')
       
  <div class="#" role="main">
          <!-- top tiles -->
          <div class="card">
            <h5 class="card-header">List Data Pegawai  <a href="{{url('datapegawai/create')}}" class="btn btn-outline-primary" style="float: right;">Tambah Data</a></h5>
                <div class="card-body">
                <table id="data_pegawai" class="table table-bordered table-striped" center>
              		<thead>                      
              			<tr align="center">
              				<th>No</th>
              				<th>Nomor Absen</th>
              				<th>Name</th>
              				<th>Jabatan</th>
              				<th>Tanggal Masuk</th>
                      <th>Aksi </th>				
              			</tr>
              		</thead>
              	<tbody>
		
          @foreach($data as $p)
            <tr align="center">
    				  <th scope="row">{{ $loop->iteration }}</th>
					<td>{{ $p->Nomor_Absen}}</td>  
    			  	<td>{{ $p->Name}}</td>
					<td>{{ $p->Jabatan_Id}}</td>
					<td>{{ $p->Tanggal_Masuk}}</td>
					
              <td>
			  <a href="{{url('datapegawai/detail')}}" class="btn btn-outline-primary btn-sm">Detail</a>
			  
              <a href="{{url('datapegawai/'.$p->id.'/edit')}}" class="btn btn-outline-warning btn-sm">Edit</a>
              
              <a href="{{url('datapegawai/'.$p->id.'/hapus')}}" class="btn btn-outline-danger btn-sm">Hapus</a>
              
              </td>	
		        </tr>
	       @endforeach
    	</tbody>
    	</table>
    	
    	
@endsection




@section('js')
<script>
	$(document).ready(function () {
	   $('#data_pegawai').DataTable();
   });
</script>

@endsection