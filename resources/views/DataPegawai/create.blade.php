@extends('layout.app')
@section('content')
 
 <!-- page content -->
 
  <!-- top tiles -->
  <div class="card">
    <h5 class="card-header">Buat Data Pegawai  <a href="{{url('datapegawai')}}" class="btn btn-outline-danger" style="float: right;">Batal</a>
     </h5> 
        <div class="card-body">
     
        <div class="card-body">
            <form method="POST" action="{{ url('datapegawai') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="Name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                  <label for="Nomor_Absen" class="col-md-4 col-form-label text-md-right">{{ __('Nomor Absen') }}</label>

                  <div class="col-md-6">
                      <input id="Nomor_Absen" type="int" class="form-control" name="Nomor_Absen" required autocomplete="Nomor_Absen">
                  </div>
              </div>     
              
              <div class="form-group row">
                <label for="Jabatan_Id" class="col-md-4 col-form-label text-md-right">{{ __('Jabatan') }}</label>

                <div class="col-md-6">
                    {{-- <input id="Jabatan_Id" type="int" class="form-control" name="Jabatan_Id" required autocomplete="Jabatan_Id"> --}}
                
                <select class="form-control @error('roles') is-invalid @enderror" name="Jabatan_Id" type="int">
                           
                    <option value="">Pilih Jabatan</option>
                    <option value="4">Tata Usaha</option>
                    <option value="5">Bendahara</option>
                 </select>
                </div>
            </div>   
            
            <div class="form-group row">
                <label for="Tanggal Masuk'" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Masuk') }}</label>

                <div class="col-md-6">
                    <input id="Tanggal Masuk'" type="date" class="form-control" name="Tanggal_Masuk" required autocomplete="Tanggal Masuk'">
                </div>
            </div>   
            </div>
            
              <br>
              <br>
              <br>
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-outline-primary">
                            {{ __('Tambah Data ') }}
                        </button>
                    </div>
                </div>
              
            </form>
        </div>
    </div>
   
<!-- /page content -->

@endsection