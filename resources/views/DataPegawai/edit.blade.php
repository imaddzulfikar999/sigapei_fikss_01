@extends('layout.app')
@section('content')
     <!-- page content -->
     
          <div class="card">
            <h5 class="card-header">Edit Data Pegawai</h5>
                <div class="card-body">
                <form action="{{url('datapegawai/'.$edit->id)}}" method="POST">

                {{ csrf_field() }}
                @method('PUT')
    
                <div class="mb-3">
                    <label for="fromGroupExamleInput" class="form-label">Nama </label>
                    <input type="text" class="form-control" id="fromGroupExamleInput" name="Name" value="{{$edit->Name}}" placeholder="Edit Nama Pegawai">
                </div>
                <div class="mb-3">
                  <label for="fromGroupExamleInput" class="form-label">Nomor_Absen </label>
                  <input type="int" class="form-control" id="fromGroupExamleInput" name="Nomor_Absen" value="{{$edit->Nomor_Absen}}" placeholder="Edit Tunjangan Insentif">
                 </div>
                 <div class="mb-3">
                    <label for="fromGroupExamleInput" class="form-label">Nama Jabatan </label>
                    <input type="int" class="form-control" id="fromGroupExamleInput" name="Jabatan_Id" value="{{$edit->Jabatan_Id}}" placeholder="Edit Nama Jabatan Id">
                </div>
                <div class="mb-3">
                  <label for="fromGroupExamleInput" class="form-label">Tanggal_Masuk</label>
                  <input type="date" class="form-control" id="fromGroupExamleInput" name="Tanggal_Masuk" value="{{$edit->Tanggal_Masuk}}" placeholder="Edit Tanggal Masuk">
                 </div>
                
                
                
                
                
                <button type="submit" class="btn btn-outline-primary" style="float: right;">Simpan Data</button>
                </form action>
          
                </div>
  
        <!-- /page content -->
            
@endsection