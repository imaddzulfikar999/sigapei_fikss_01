
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> S I G A P E I | Aplikasi Penggajian Pegawai <i class="ti-heart" aria-hidden="true"></i> by <a href="#" target="_blank">Antik21</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="{{ asset('etrain-master') }}/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="{{ asset('etrain-master') }}/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="{{ asset('etrain-master') }}/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="{{ asset('etrain-master') }}/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="{{ asset('etrain-master') }}/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="{{ asset('etrain-master') }}/js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="{{ asset('etrain-master') }}/js/owl.carousel.min.js"></script>
    <script src="{{ asset('etrain-master') }}/js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="{{ asset('etrain-master') }}/js/slick.min.js"></script>
    <script src="{{ asset('etrain-master') }}/js/jquery.counterup.min.js"></script>
    <script src="{{ asset('etrain-master') }}/js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="{{ asset('etrain-master') }}/js/custom.js"></script>
</body>

</html>