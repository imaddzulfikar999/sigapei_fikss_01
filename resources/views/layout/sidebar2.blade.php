<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a class="nav-link" href="{{ url('dashboard') }}"><i class="fa fa-home" ></i> Dashboard </a> </li>
                  
                  @if (Auth::user()->role_id == 1)
                        <li><a class="nav-link" href="{{ url('dataPegawai') }}"><i class="fa fa-edit"></i> Data Pegawai</a> </li>
                        <li><a><i class="fa fa-clone"></i> Data Absensi Pegawai<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="{{ url('rekapankehadiran') }}">Rekapan Kehadiran</a></li>
                              <li><a  href="{{ url('rekapanketerlambatan') }}">Rekapan Keterlambatan</a></li>
                              <li><a href="{{ url('datalog') }}">Data Log</a></li>
                          </ul>
                        </li> 
                        <li><a class="nav-link" href="{{ url('dataPenggajian') }}"><i class="fa fa-table"></i> Data Penggajian</a>  </li>
                        <li><a class="nav-link" href="{{ url('importdataabsensi') }}"><i class="fa fa-bar-chart-o"></i> Import Data Absensi </a></li>
                        <li><a><i class="fa fa-clone"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                              <ul class="nav child_menu">
                                  <li><a href="{{ url('jabatan') }}">Jabatan</a></li>
                                  <li><a  href="{{ url('insentif') }}">Insentif</a></li>
                                  <li><a href="{{ url('roles') }}">Roles</a></li>
                                  <li><a href="{{ route('register') }}">Buat Account</a></li>
                              </ul>
                          </li>
                    @elseif(Auth::user()->role_id == 2)
                          <li><a><i class="fa fa-clone"></i>Laporan Absensi Pegawai<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('rekapankehadiran') }}">Rekapan Kehadiran</a></li>
                                <li><a  href="{{ url('rekapanketerlambatan') }}">Rekapan Keterlambatan</a></li>
                            </ul>
                          </li> 
                          <li><a class="nav-link" href="{{ url('dataPegawai') }}"><i class="fa fa-edit"></i>Laporan Penggajian</a> </li>
                    
                          @elseif(Auth::user()->role_id == 3)
                          <li><a class="nav-link" href="{{ url('dataPegawai') }}"><i class="fa fa-edit"></i>Laporan Data Pegawai</a> </li>
                          <li><a><i class="fa fa-clone"></i>Laporan Absensi Pegawai<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('rekapankehadiran') }}">Rekapan Kehadiran</a></li>
                                <li><a  href="{{ url('rekapanketerlambatan') }}">Rekapan Keterlambatan</a></li>
                            </ul>
                          </li> 
                          <li><a class="nav-link" href="{{ url('dataPegawai') }}"><i class="fa fa-edit"></i>Laporan Penggajian</a> </li>       
                    
                          @elseif(Auth::user()->role_id == 4)                          
                              <li><a><i class="fa fa-clone"></i>Laporan Absensi Pegawai<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('rekapankehadiran') }}">Rekapan Kehadiran</a></li>
                                    <li><a  href="{{ url('rekapanketerlambatan') }}">Rekapan Keterlambatan</a></li>
                                </ul>
                              </li> 
                              <li><a class="nav-link" href="{{ url('dataPegawai') }}"><i class="fa fa-edit"></i>Laporan Penggajian</a> </li>
                              
                      
                  @else
                          
                  @endif
                </ul>
              </div>
            </div>