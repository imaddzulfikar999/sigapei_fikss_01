@extends('layout.app')
@section('content')
    	    
  <div class="#" role="main">
    <!-- top tiles -->
    <div class="card">
    
      <h5 class="card-header">List Rekapan Kehadiran Pegawai </h5>
          <div class="card-body">
            <h6>Pilih Bulan Dan Tahun  </h6>
                
             <div class="col-md-4">
                <select class="form-control @error('roles') is-invalid @enderror" name="Pilih Bulan">
                     
                  <option value="">Pilih Bulan</option>
                  <option value="Januari">Januari</option>
                  <option value="Februari">Februari</option>
                  <option value="Maret">Maret</option>
                  <option value="April">April</option>
                
                
                 </select>
             </div>
                <div class="col-md-4">
                  <select class="form-control @error('roles') is-invalid @enderror" name="Pilih Tahun">
                       
                    <option value="">Pilih Tahun</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                   </select>
                   </div>
             </div>
                
                
               <table  id="data_log" class="table table-bordered table-striped" center>
                    <thead>                      
                        <tr align="center">
                            <th rowspan="2">No</th>
                            <th rowspan="2">Name</th>
                            <th rowspan="2">Jabatan</th>
                            <th rowspan="2">TMT</th>
                            <th colspan="2">Masa Kerja</th>
                            <th rowspan="2">Hadir</th>
                            <th rowspan="2">Tepat Waktu </th>
                            <th rowspan="2">Aksi </th>				
                        </tr>
                        <tr align="center">
                          
                          <td>Tahun</td>
                          <td>Bulan</td>			
                      </tr>
                    </thead>
                       <tbody>
                        
				          @foreach($kehadiran as $k)
                  <tr align="center">
                        {{-- <th>{{ $k->Nomor_Absen}}</th>
                        <td>{{ $k->nama}}</td>
                        <td>{{ $k->Jabatan}}</td>
                        <td>{{ $k->tanggal_masuk}}</td>
                        <td>{{ $k->Tahun}}</td>
                        <td>{{ $k->Bulan}}</td>
                        <td>{{ $k->jml_hadir}}</td>
                        <td>{{ $k->jml_tepat_waktu}}</td> --}}
                        
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        
                    <td>
                    
                      <a href="/rekapankehadiran/${data}" class="btn btn-outline-primary btn-sm">Detail</a> 
                                    
                    </td>	
                  </tr>
               @endforeach
                        
                      </tbody>
            </table>
            
    </div>
  </div>
  
@endsection



@section('js')
<script>
	$(document).ready(function () {
	   $('#data_log').DataTable();
   });
</script>

@endsection