@extends('layout.app')
@section('content')
       
  <div class="#" role="main">
          <!-- top tiles -->
          <div class="card">
            <h5 class="card-header">List Rekapan Data Penggajian Pegawai</h5>
            
             
            <div class="card-body ">
              <h6>Pilih Bulan Dan Tahun  </h6>
                
                <div class="col-md-4">
                    <select class="form-control @error('roles') is-invalid @enderror" name="Pilih Bulan">
                         
                      <option value="">Pilih Bulan</option>
                      <option value="Januari">Januari</option>
                      <option value="Februari">Februari</option>
                      <option value="Maret">Maret</option>
                      <option value="April">April</option>
                    
                    
                     </select>
                </div>
                <div class="col-md-4">
                  <select class="form-control @error('roles') is-invalid @enderror" name="Pilih Tahun">
                       
                    <option value="">Pilih Tahun</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                   </select>
                   </div>
                   
						   <h4>Total Anggaran : Rp. </h4>
                </div>
				  
                <table id="data_penggajian" class="table table-bordered table-striped" table-responsive>
              		<thead>                      
              			<tr align="center">
              				<th rowspan="2">No</th>
              				<th rowspan="2">Name</th>
              				<th rowspan="2">Gaji Pokok </th>
              				<th rowspan="2">Tunjangan</th>
              				<th colspan="2">Insentif</th>
              				<th rowspan="2">Bonus</th>
              				<th rowspan="2">Gaji Bersih</th>
                            <th rowspan="2">Aksi </th>				
              			</tr>
              			<tr align="center">
                            <th >Transport</th>
                            <th >Konsumsi</th>
                            
                        </tr>
              		</thead>
              	<tbody>
		
    	</tbody>
    	</table>
				</div>
		  </div>
  </div>
    	
@endsection


@section('js')
<script>
	$(document).ready(function () {
	   $('#data_penggajian').DataTable();
   });
</script>

@endsection