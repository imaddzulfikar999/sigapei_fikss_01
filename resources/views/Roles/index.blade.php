@extends('layout.app')
@section('content')
       
  <div class="#" role="main">
          <!-- top tiles -->
          <div class="card">
            <h5 class="card-header">List Data Roles  </h5>
                <div class="card-body">
	                <table id="masterdata_role" class="table table-bordered table-striped" center>
		              		<thead>                      
		              			<tr align="center">
		              				<th>No</th>
		              				<th>Nama Role</th>	
		              			</tr>
		              		</thead>
	              	      <tbody>
			
					          @foreach($Roles as $r)
					            <tr align="center">
					    				  <th scope="row">{{ $loop->iteration }}</th>
					    			  	<td>{{ $r->name}}</td>
					            </tr>
						       @endforeach
	            	       </tbody>
	    	       </table>
				</div>
		  </div>
  </div>
    	
    	
@endsection



@section('js')
<script>
	$(document).ready(function () {
	   $('#masterdata_role').DataTable();
   });
</script>

@endsection