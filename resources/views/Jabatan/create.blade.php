@extends('layout.app')
@section('content')
 
 <!-- page content -->
 
  <!-- top tiles -->
  <div class="card">
    <h5 class="card-header">Buat Data Jabatan   <a href="{{url('masterdata/jabatan')}}" class="btn btn-outline-danger" style="float: right;">Batal</a>
     </h5> 
        <div class="card-body">
     
        <div class="card-body">
            <form method="POST" action="{{ url('masterdata/jabatan') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                  <label for="tj_jabatan" class="col-md-4 col-form-label text-md-right">{{ __('Tunjangan jabatan') }}</label>

                  <div class="col-md-6">
                      <input id="tj_jabatan" type="int" class="form-control" name="tj_jabatan" required autocomplete="tj_jabatan">
                  </div>
              </div>         
            </div>
            
              <br>
              <br>
              <br>
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-outline-primary">
                            {{ __('Tambah Data ') }}
                        </button>
                    </div>
                </div>
              
            </form>
        </div>
    </div>
   
<!-- /page content -->

@endsection