@extends('layout.app')
@section('content')
     <!-- page content -->
     
          <div class="card">
            <h5 class="card-header">Edit Data Jabatan</h5>
                <div class="card-body">
                <form action="{{url('masterdata/jabatan/'.$edit->id)}}" method="POST">

                {{ csrf_field() }}
                @method('PUT')
    
                <div class="mb-3">
                    <label for="fromGroupExamleInput" class="form-label">Nama jabatan </label>
                    <input type="text" class="form-control" id="fromGroupExamleInput" name="name" value="{{$edit->name}}" placeholder="Edit Nama jabatan">
                </div>
                <div class="mb-3">
                  <label for="fromGroupExamleInput" class="form-label">Tunjangan Jabatan </label>
                  <input type="text" class="form-control" id="fromGroupExamleInput" name="tj_jabatan" value="{{$edit->tj_jabatan}}" placeholder="Edit Tunjangan jabatan">
                 </div>
                
                
                <button type="submit" class="btn btn-outline-primary" style="float: right;">Simpan Data</button>
                </form action>
          
            </div>
  
        <!-- /page content -->
            
@endsection