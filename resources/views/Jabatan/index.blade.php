@extends('layout.app')
@section('content')
       
  <div class="#" role="main">
          <!-- top tiles -->
          <div class="card">
            <h5 class="card-header">List Data Jabatan  <a href="{{url('masterdata/jabatan/create')}}" class="btn btn-outline-primary" style="float: right;">Tambah Data</a></h5>
                <div class="card-body">
                <table id="masterdata_jabatan" class="table table-bordered table-striped" center>
              		<thead>                      
              			<tr align="center">
              				<th>No</th>
              				<th>Name</th>
              				<th>Tunjangan Jabatan</th>
                      <th>Aksi </th>				
              			</tr>
              		</thead>
              	<tbody>
		
          @foreach($data as $i)
            <tr align="center">
    				  <th scope="row">{{ $loop->iteration }}</th>
    			  	<td>{{ $i->name}}</td>
					<td>{{ $i->tj_jabatan}}</td>
              <td>
              
              <a href="{{url('masterdata/jabatan/'.$i->id.'/edit')}}" class="btn btn-outline-success btn-sm">Edit</a>
              
              <a href="{{url('masterdata/jabatan/'.$i->id.'/hapus')}}" class="btn btn-outline-danger btn-sm">Hapus</a>
              
              </td>	
		        </tr>
	       @endforeach
    	</tbody>
    	</table>
    	
    	
@endsection


@section('js')
<script>
	$(document).ready(function () {
	   $('#masterdata_jabatan').DataTable();
   });
</script>

@endsection