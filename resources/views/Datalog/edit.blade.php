@include('Template.head')
            <!-- menu profile quick info -->
            @include('Template.profil')
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            @include('Template.sidebar')
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings"></a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen"> </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock"> </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        @include('Template.topnavigator')
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="card">
            <h5 class="card-header">Edit Data Jabatan </h5>
                <div class="card-body">
                <form action="{{url('jabatan/'.$edit->id)}}" method="POST">

                {{ csrf_field() }}
                @method('PUT')
    
                <div class="mb-3">
                    <label for="fromGroupExamleInput" class="form-label">Nama Jabatan</label>
                    <input type="text" class="form-control" id="fromGroupExamleInput" name="name" value="{{$edit->name}}" placeholder="Input Nama Jabatan">
                </div>
                <div class="mb-3">
                    <label for="fromGroupExamleInput" class="form-label">Tunjangan Jabatan</label>
                    <input type="text" class="form-control" id="fromGroupExamleInput" name="tunjangan_jabatan" value="{{$edit->tj_jabatan}}" placeholder="Input Tunjangan Jabatan">
                </div>
                
                <button type="submit" class="btn btn-primary" style="float: right;">Simpan Data</button>
                </form action>
                   
                          
             
          <!-- /top tiles -->

             
            </div>
          
        </div>
        <!-- /page content -->
            
        <!-- footer content -->
        <footer>
        @include('Template.footer');
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    @include('Template.script');
  </body>
</html>
