@extends('layout.app')
@section('content')
       
  <div class="#" role="main">
          <!-- top tiles -->
          <div class="card">
			
            <div class="card-header">List Data loqs Pegawai </div>
                <div class="card-body">
					<h6>Pilih Bulan Dan Tahun  </h6>
                
					<div class="col-md-4">
						<select class="form-control @error('roles') is-invalid @enderror" name="Pilih Bulan">
							 
						  <option value="">Pilih Bulan</option>
						  <option value="Januari">Januari</option>
						  <option value="Februari">Februari</option>
						  <option value="Maret">Maret</option>
						  <option value="April">April</option>
						
						
						 </select>
					</div>
					<div class="col-md-4">
					  <select class="form-control @error('roles') is-invalid @enderror" name="Pilih Tahun">
						   
						<option value="">Pilih Tahun</option>
						<option value="2020">2020</option>
						<option value="2021">2021</option>
						<option value="2022">2022</option>
						<option value="2023">2023</option>
						<option value="2024">2024</option>
						<option value="2025">2025</option>
						<option value="2026">2026</option>
					   </select>
					   </div>
					   
							   <h4>Total Data Log:  </h4>
					</div>
	                <table id="data_log" class="table table-bordered table-striped w-100"  >
	              		<thead>    
								<tr align="center">
									
									<th rowspan="2">Tanggal</th>
									<th rowspan="2">Nama</th>
									<th colspan="2">Waktu Absensi</th>
								
								</tr>
								<tr align="center">
									<td>Jam Masuk</td>
									<td>Jam Keluar</td>							
								</tr>
															
	            		</thead>
		              <tbody>
		              	</tbody>
	              	</table>
              	
              	</div>
            </div>
  </div>
	  	
@endsection

@section('js')
<script>
	$(document).ready(function () {
	   $('#data_log').DataTable({
		   processing: true,
		   serverSide: true,
		   ajax: '{{url("datalog/getdata")}}',
		   columns: [
			   
			   {
				   data: 'tanggal',
				   name: 'tanggal',
				   "sClass": "text-center"
			   },
			   {
				   data: 'nama',
				   name: 'nama',
				   "sClass": "text-center"
			   },
			   {
				   data: 'jam_masuk',
				   name: 'jam_masuk',
				   "sClass": "text-center"
			   },
			   {
				   data: 'jam_keluar',
				   name: 'jam_keluar',
				   "sClass": "text-center"
			   }
			   
				              
			  
		   ]
	   });
   });
</script>

@endsection