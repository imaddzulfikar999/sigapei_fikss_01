  <form action="{{ route('upload.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
  <div class="custom-file">
    <input type="file" class="custom-file-input" id="customFile" name="fileupload">
    
  <label class="custom-file-label" for="customFile">Choose file</label>
  <button type="submit">Upload</button>
  </div>
</form>