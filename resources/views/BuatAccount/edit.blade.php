@extends('layout.app')
@section('content')
 
 <!-- page content -->
 
  <!-- top tiles -->
  <div class="card">
    <h5 class="card-header">Edit Akun <a href="{{url('masterdata/buataccount')}}" class="btn btn-outline-danger" style="float: right;">Batal</a>
     </h5> 
        <div class="card-body">
     
        <div class="card-body">
            <form method="POST" action="{{ url('masterdata/buataccount/' .$edit->id)}}">
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-3">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $edit->name }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $edit->email }}" required autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>              
                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Ganti Password') }}</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password" >
                    </div>
                </div>
                <div class="form-group row" >
                   <label for="roles" class="col-md-4 col-form-label text-md-right">{{ __('Pilih Roles') }}</label>

                    <div class="col-md-6" >
                        <!-- <input id="roles" type="roles" class="form-control @error('roles') is-invalid @enderror" name="roles" required autocomplete="roles"> -->
                      <select class="form-control @error('roles') is-invalid @enderror" name="roles">
                        <option value="">Pilih Role</option>
                           @if ($edit->role_id==1)
                             <option value="1" selected>Tata Usaha</option>
                             <option value="2">Bendahara</option>
                             <option value="3">Kepala Sekolah</option>
                             <option value="4">Pegawai</option>
                             
                                 @elseif ($edit->role_id==2)
                                   <option value="1">Tata Usaha</option>
                                   <option value="2" selected >Bendahara</option>
                                   <option value="3">Kepala Sekolah</option>
                                   <option value="4">Pegawai</option>
                                      
                                   @elseif ($edit->role_id==3)
                                     <option value="1">Tata Usaha</option>
                                     <option value="2">Bendahara</option>
                                     <option value="3" selected >Kepala Sekolah</option>
                                     <option value="4">Pegawai</option>
                                   
                                   @elseif ($edit->role_id==4)
                                   <option value="1">Tata Usaha</option>
                                   <option value="2">Bendahara</option>
                                   <option value="3">Kepala Sekolah</option>
                                   <option value="4" selected >Pegawai</option>
                           @endif
                           
                      </select>
                      
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                   
                    
                </div>
                <div class="form-group row">
                  <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Pilih Status') }}</label>

                  <div class="col-md-6">
                  <select class="form-control @error('roles') is-invalid @enderror" name="status">
                      <option value="">Pilih Status</option>
                      
                            @if ($edit->status== "Aktif")
                              <option value="Aktif" selected >Aktif</option>
                              <option value="Tidak Aktif">Tidak Aktif</option>
                                                               
                                @elseif ($edit->status=="Tidak Aktif")
                                  <option value="Aktif">Aktif</option>
                                  <option value="Tidak Aktif" selected >Tidak Aktif</option>
                                  
                                                               
                                @else 
                                  <option value="Aktif">Aktif</option>
                                  <option value="Tidak Aktif">Tidak Aktif</option>
                                                                    
                           @endif
                      
                   </select>
                
                  @error('password')
                      <span class="invalid-feedback" status="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                  
                  </div>
              </div>
              <div class="form-group row">
                <label for="avatar" class="col-md-4 col-form-label text-md-right">{{ __('Tambah Gambar') }}</label>

                <div class="col-md-6">
                  <input class="form-control @error('avatar') is-invalid @enderror" name="avatar" name="file">
                  
                    <input type="file" >
                
                </div>
                
            </div>
            
              <br>
              <br>
              <br>
                 
              <button type="submit" class="btn btn-outline-primary" style="float: right;">Simpan Data</button>
            </form action>
      
              
            </form>
        </div>
    </div>
   
<!-- /page content -->

@endsection