@extends('layout.app')
@section('content')
       
        <!-- top tiles -->
        
          <div class="card">
            <h5 class="card-header">Buat Account Baru <a href="{{url('masterdata/buataccount/create')}}" class="btn btn-outline-primary" style="float: right;">Tambah Data</a></h5>
                <div class="card-body">
                <table id="buat_account" class="table table-bordered table-striped" center>
              		<thead>                      
              			<tr align="center">
              				<th>No</th>
              				<th>Name</th>
              				<th>Email</th>
              				<th>Role</th>
              				<th>Avatar</th>
              				<th>Status</th>
		              	    <th>Aksi </th>				
		              	</tr>
              		</thead>
                	<tbody>
		
				          @foreach($Users as $u)
				            <tr align="center">
				    				<th scope="row">{{ $loop->iteration }}</th>
				    			  	<td>{{ $u->name}}</td>
				    			  	<td>{{ $u->email}}</td>
				    			    <td>{{ $u->role->name}}</td>
				    			    <td>{{ $u->avatar}}</td>
				    			    <td>{{ $u->status}}</td>
				              <td>
				              
				              <a href="{{url('masterdata/buataccount/'.$u->id.'/edit')}}" class="btn btn-outline-warning btn-sm">Edit</a>
				              
				              <a href="{{url('masterdata/buataccount/'.$u->id.'/hapus')}}" class="btn btn-outline-danger btn-sm">Hapus</a>
				              
				              </td>	
						        </tr>
					       @endforeach 
			    	</tbody>
			    </table>
			    	
    	
				</div>
			</div>
		  
@endsection


@section('js')
<script>
	$(document).ready(function () {
	   $('#buat_account').DataTable();
   });
</script>

@endsection