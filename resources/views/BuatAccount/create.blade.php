@extends('layout.app')
@section('content')
 
 <!-- page content -->
 
  <!-- top tiles -->
  <div class="card">
    <h5 class="card-header">Buat Akun Baru   <a href="{{url('masterdata/buataccount')}}" class="btn btn-outline-danger" style="float: right;">Batal</a>
     </h5> 
        <div class="card-body">
     
        <div class="card-body">
            <form method="POST" action="{{ url('masterdata/buataccount') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>
                <div class="form-group row" >
                   <label for="roles" class="col-md-4 col-form-label text-md-right">{{ __('Pilih Roles') }}</label>

                    <div class="col-md-6" >
                        <!-- <input id="roles" type="roles" class="form-control @error('roles') is-invalid @enderror" name="roles" required autocomplete="roles"> -->
                      <select class="form-control @error('roles') is-invalid @enderror" name="roles">
                           
                            <option value="">Pilih Role</option>
                            <option value="1">Tata Usaha</option>
                            <option value="2">Bendahara</option>
                            <option value="3">Kepala Sekolah</option>
                            <option value="4">Pegawai</option>
                      </select>
                      
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                   
                    
                </div>
                <div class="form-group row">
                  <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Pilih Status') }}</label>

                  <div class="col-md-6">
                    <select class="form-control @error('roles') is-invalid @enderror" name="status">
                           
                      <option value="">Pilih Status</option>
                      <option value="Aktif">Aktif</option>
                      <option value="Tidak Aktif">Tidak Aktif</option>
                   </select>
                
                  @error('password')
                      <span class="invalid-feedback" status="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                  
                  </div>
              </div>
              <div class="form-group row">
                <label for="avatar" class="col-md-4 col-form-label text-md-right">{{ __('Tambah Gambar') }}</label>

                <div class="col-md-6">
                  <input class="form-control @error('avatar') is-invalid @enderror" name="avatar" name="file">
                  
                    <input type="file" >
                
                </div>
                
            </div>
            
              <br>
              <br>
              <br>
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-outline-primary">
                            {{ __('Tambah Data ') }}
                        </button>
                    </div>
                </div>
              
            </form>
        </div>
    </div>
   
<!-- /page content -->

@endsection