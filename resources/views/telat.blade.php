@extends('layout.app')
@section('content')
    
<div class="#" role="main">
    <!-- top tiles -->
    <div class="card">
      <h5 class="card-header">Detail Rekapan Data Keterlambatan <a href="#" class="btn btn-outline-danger btn-sm" style="float: right;">Kembali</a></h5>  
          <div class="card-body">
          <h5 class="card-title">Nama Pegawai : </h5> 
          <h5 class="card-title"> Bulan Tahun :  </h5>
            
            <table class="table table-bordered table-striped" center>
                     <thead>                      
                        <tr align="center">
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Jam Masuk</th>
                            			
                        </tr>
                    </thead>
                 <tbody>
                </tbody>
            </table>
           
          </div>
    </div>
    
  </div>
    
@endsection
