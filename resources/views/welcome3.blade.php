@include('layout.auth.header')


    <!-- banner part start-->
    <section class="banner_part">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-xl-6">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h1>S I G A P E I</h1>
                            <h2>APLIKASI PENGGAJIAN PEGAWAI</h2>
                            <p> It is hoped that with the employee payroll application,
                                can speed up the payroll process of employees who are
                                at MI Terpadu Al Ishlah Gorontalo, as well as making it easier for the Treasurer
                                in recapitulation of salaries, and recapitulation of financial statements.
                                So that the treasurer can make remuneration based on the number of employee attendance.</p>
                            <a href="login" class="btn_2">L O G I N</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <!-- member_counter counter start -->
    <section class="member_counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">1024</span>
                        <h4>All Pegawai</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">960</span>
                        <h4> All Students</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">1020</span>
                        <h4>Online Students</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">820</span>
                        <h4>Ofline Students</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- member_counter counter end -->


    @include('layout.auth.footer')